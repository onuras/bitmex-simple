module.exports = {
  baseUrl: './',
  devServer: {
    proxy: {
      '^/testnet/api': {
        target: 'https://testnet.bitmex.com/',
        pathRewrite: { '^/testnet': '/' },
        changeOrigin: true,
      },
      '^/live/api': {
        target: 'https://www.bitmex.com/',
        pathRewrite: { '^/live': '/' },
        changeOrigin: true,
      },
    },
  },
}
