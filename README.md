# bitmex-simple

[![pipeline status](https://gitlab.com/onuras/bitmex-simple/badges/master/pipeline.svg)](https://gitlab.com/onuras/bitmex-simple/commits/master)

Realtime, lightweight, mobile-first, profit-loss oriented Bitcoin trading
client for BitMEX.  Using directly BitMEX websocket and rest API. Live
version is available in <https://bitmex-simple.onur.im/>

[![ss](https://i.imgur.com/si95hNLl.png)](https://i.imgur.com/si95hNL.png)


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```
