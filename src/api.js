// bitmex websocket and rest api

'use strict';
import axios from 'axios';
import crypto from 'crypto';
import { Snackbar } from 'buefy/dist/components/snackbar';
import { store, types } from './store.js';


class API {
  // websocket
  socket = null;

  key = '';
  secret = '';
  testnet = false;

  // axios instance
  instance = null;


  constructor(key, secret, testnet) {
    this.key = key;
    this.secret = secret;
    this.testnet = testnet;

    this.instance = axios.create({
      baseURL: testnet ? '/testnet/api/v1' : '/live/api/v1',
    });
    this.instance.interceptors.request.use(c => this.axiosRequestInterceptor(c));
    this.instance.interceptors.response.use(r => r, e => this.axiosResponseInterceptor(e));

    this.connectSocket();
  }


  connectSocket() {
    // remove previous listeners if our socket is previously connected
    if (this.socket) {
      this.socket.onopen = this.socket.onclose =
        this.socket.onmessage = this.socket.onerror = null;
    }

    this.socket =  this.testnet ?
                   new WebSocket("wss://testnet.bitmex.com/realtime") :
                   new WebSocket("wss://www.bitmex.com/realtime");

    // socket events
    this.socket.onopen = e => this.onopen(e);
    this.socket.onclose = e => this.onclose(e);
    this.socket.onmessage = e => this.onmessage(e);
    this.socket.onerror = () => this.reconnect();
  }


  subscribe() {
    this.socket.send(JSON.stringify({
      'op': 'subscribe',
      'args': [
        'instrument:XBTUSD',
        'order',
        'position',
        'wallet',
        'margin',
      ]
    }));
  }


  authenticate() {
    const expires = ((new Date()).getTime() / 1000 | 0) + 3600;
    const signature = crypto.createHmac('sha256', this.secret)
                            .update('GET/realtime' + expires)
                            .digest('hex');
    this.socket.send(JSON.stringify({
      op: 'authKeyExpires',
      args: [
        this.key,
        expires,
        signature
      ],
    }));
  }


  // axios interceptor to calculate headers
  axiosRequestInterceptor(config) {
    const verb = config.method.toUpperCase();
    const path = config.baseURL.replace('/testnet', '').replace('/live', '') + config.url;
    const expires = (new Date().getTime() / 1000 | 0) + 60; // 1 min in the future
    const signature = crypto.createHmac('sha256', this.secret)
      .update(verb + path + expires + (config.data ? JSON.stringify(config.data) : ''))
      .digest('hex');

    config.headers = {
      'Content-Type' : 'application/json',
      'Accept': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      'Api-Expires': expires,
      'Api-Key': this.key,
      'Api-Signature': signature
    };

    return config;
  }


  axiosResponseInterceptor(error) {
    try {
      const name = error.response.data.error.name;
      const message = error.response.data.error.message;
      Snackbar.open({
        message: `${name}: ${message}`,
        type: 'is-danger'
      });
    } catch(e) {
      Snackbar.open({
        message: 'There is been an error while processing request',
        type: 'is-danger'
      });
    }
    return Promise.reject(error);
  }


  history() {
    return this.instance.get('/user/walletHistory')
      .then(response => store.commit(types.SET_HISTORY, response.data));
  }


  cancelOrder(orderID) {
    return this.instance.delete('/order', { data: { orderID: orderID } });
  }


  reconnect() {
    setTimeout(() => this.connectSocket(), 1000);
  }


  onopen() {
    this.authenticate();
  }


  onclose(e) {
    if (e.code != 1000) {
      this.reconnect();
    }
  }


  onmessage(event) {
    // for debug
    if (process.env.NODE_ENV != 'production') {
      //console.log(event.data);
    }


    const data = JSON.parse(event.data);

    // login error
    if (data.status == 401) {
      store.commit(types.SET_LOGININ, false);
      store.commit(types.SET_ERROR, data.error);
      return;
    }


    // try to update page title
    if (data.table == "instrument" &&
        data.data[0].symbol == "XBTUSD" &&
        data.data[0].lastPrice) {
      document.title = `${data.data[0].lastPrice | 0} Bitmex Simple`;
    }


    // check authentication
    if (data.success == true && data.request.op == "authKeyExpires") {
      // successfully login
      store.commit(types.SET_LOGINREQUIRED, false);
      store.dispatch('save_options');
      this.subscribe();
    }


    if (data.action == "partial") {
      store.commit(types.SET_TABLE, {
        table: data.table,
        data: data
      });
    }

    else if (data.action == "insert") {
      data.data.forEach(item =>
        store.commit(types.INSERT_TABLE, {
          table: data.table,
          data: item
        }));
    }

    else if (data.action == "update") {
      data.data.forEach(item =>
        store.commit(types.UPDATE_TABLE, {
          table: data.table,
          data: item,
        }));
    }

    else if (data.action == "delete") {
      data.data.forEach(item =>
        store.commit(types.DELETE_TABLE, {
          table: data.table,
          data: item,
        }));
    }

  }
}


export default API;
