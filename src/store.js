import Vue from 'vue'
import Vuex from 'vuex'
import API from './api';

Vue.use(Vuex)

const types = {
  SET_API: 'SET_API',
  SET_LOGININ: 'SET_LOGININ',
  SET_LOGINREQUIRED: 'SET_LOGINREQUIRED',
  SET_ERROR: 'SET_ERROR',
  SET_HISTORY: 'SET_HISTORY',
  SET_TABLE: 'SET_TABLE',
  SET_OPTION: 'SET_OPTION',
  INSERT_TABLE: 'INSERT_TABLE',
  UPDATE_TABLE: 'UPDATE_TABLE',
  DELETE_TABLE: 'DELETE_TABLE',
};

const state = {
  api: null,

  history: [],
  tables: {},

  error: '',

  loginRequired: true,
  loginin: false,
  saveKey: false,
  testnet: false,

  options: {
    key: '',
    secret: '',
    testnet: false,
  },
};

const getters = {
  price(state) {
    try {
      return state.tables.instrument.data.find(i => i.symbol == 'XBTUSD').lastPrice;
    } catch(e) {
      return 0;
    }
  },
  position(state) {
    try {
      return state.tables.position.data.find(p => p.symbol == 'XBTUSD');
    } catch(e) {
      return null;
    }
  },
  positionEntryPrice(state) {
    try {
      return state.tables.position.data.find(p => p.symbol == 'XBTUSD').avgCostPrice;
    } catch(e) {
      return 0;
    }
  },
  positionQuantity(state) {
    try {
      return state.tables.position.data.find(p => p.symbol == 'XBTUSD').currentQty;
    } catch(e) {
      return 0;
    }
  },
  pnl(state) {
    const quantity = getters.positionQuantity(state);
    const entryPrice = getters.positionEntryPrice(state);
    const price = getters.price(state);
    if (price == 0) {
      return 0;
    }
    return (((quantity / entryPrice) -
             (quantity / price))
            * 1000).toFixed(3);
  },
  closePrice(state) {
    try {
      return getters.limitOrder(state).price;
    } catch(e) {
      return 0;
    }
  },
  stopPrice(state) {
    try {
      return getters.stopOrder(state).stopPx;
    } catch(e) {
      return 0;
    }
  },
  limitOrder(state) {
    try {
      return getters.orders(state).find(o => o.ordType == 'StopLimit' || o.ordType == 'StopLimit');
    } catch(e) {
      return null;
    }
  },
  stopOrder(state) {
    try {
      return getters.orders(state).find(order => order.ordType == 'Stop');
    } catch(e) {
      return null;
    }
  },
  orders(state) {
    try {
      return state.tables.order.data.filter(o => o.ordStatus != 'Filled' && o.ordStatus != 'Canceled');
    } catch(e) {
      return [];
    }
  },
  wallet(state) {
    try {
      return state.tables.wallet.data;
    } catch(e) {
      return [];
    }
  },
  margin(state) {
    try {
      return state.tables.margin.data;
    } catch(e) {
      return [];
    }
  }
};

const mutations = {
  [types.SET_API](state, api) {
    state.api = api;
  },
  [types.SET_LOGININ](state, loginin) {
    state.loginin = loginin;
  },
  [types.SET_LOGINREQUIRED](state, loginRequired) {
    state.loginRequired = loginRequired;
  },
  [types.SET_ERROR](state, error) {
    state.error = error;
  },
  [types.SET_HISTORY](state, history) {
    state.history = history;
  },
  [types.SET_OPTION](state, option) {
    for (const key in option) {
      Vue.set(state.options, key, option[key]);
    }
  },
  [types.SET_TABLE](state, { table, data }) {
    Vue.set(state.tables, table, data);
  },
  [types.INSERT_TABLE](state, { table, data }) {
    state.tables[table].data.push(data);
  },
  [types.UPDATE_TABLE](state, { table, data }) {
    if (!(table in state.tables)) {
      console.error(`Failed to update: ${table}`);
      return;
    }

    const keys = state.tables[table].keys;
    state.tables[table].data.filter(d => {
      let found = true;
      for (const key in keys) {
        if (d[keys[key]] != data[keys[key]]) {
          found = false;
          break;
        }
      }
      return found;
    }).forEach(d => {
      for (const key in data) {
        Vue.set(d, key, data[key]);
      }
    });
  },
  [types.DELETE_TABLE](state, { table, data }) {
    // TODO: implement this
    console.error(`DELETE_TABLE not implemented: ${table}`);
    console.log(data);
  },
};

const actions = {
  login({ commit }, { key, secret, testnet, remember }) {
    commit(types.SET_LOGININ, true);
    if (remember) {
      commit(types.SET_OPTION, { key: key });
      commit(types.SET_OPTION, { secret: secret });
      commit(types.SET_OPTION, { testnet: testnet });
    }
    const api = new API(key, secret, testnet);
    commit(types.SET_API, api);
  },


  cancel_all_orders({ state }) {
    return state.api.instance.delete("/order/all");
  },


  bulk_order({ state }, data) {
    return state.api.instance.post('/order/bulk', data);
  },


  // Opens or alters limit order
  // FIXME: This action used in simple action controls and should be deprecated
  alter_limit_order({ state, getters }, { price, quantity }) {
    // find limit order
    const closeOrder = getters.limitOrder;
    const quantity_ = quantity || getters.positionQuantity;
    const data = {
      symbol:"XBTUSD",
      price: price || getters.price + (quantity_ < 0 ? -0.5 : 0.5),
      orderQty: quantity_,
      execInst: getters.positionQuantity != 0 ? 'ReduceOnly' : null,
    };
    if (closeOrder) {
      const id = closeOrder.orderID;
      return state.api.instance.put('/order', {
        orderID: id,
        ...data
      });
    } else {
      return state.api.instance.post('/order', data);
    }
  },

  // Opens or changes stop order
  // FIXME: This action used in simple action controls and should be deprecated
  alter_stop_order({ state, getters }, { price, quantity }) {
    // find limit order
    const stopOrder = getters.stopOrder;
    const quantity_ = quantity || getters.positionQuantity;
    const data = {
      symbol:"XBTUSD",
      stopPx: price || getters.price + (quantity_ < 0 ? -0.5 : 0.5),
      orderQty: quantity_,
      execInst: 'Close',
    };
    if (stopOrder) {
      const id = stopOrder.orderID;
      return state.api.instance.put('/order', {
        orderID: id,
        ...data
      });
    } else {
      return state.api.instance.post('/order', data);
    }
  },

  get_history({ state }) {
    return state.api.history();
  },

  save_options({ state }) {
    localStorage.setItem('bitmex-simple', JSON.stringify(state.options));
  },

  load_options({ commit }) {
    const options = JSON.parse(localStorage.getItem('bitmex-simple'));
    for (const key in options) {
      commit(types.SET_OPTION, { [key]: options[key] });
    }
  },

  cancel_order({ state }, orderID) {
    return state.api.cancelOrder(orderID);
  },
};


const store = new Vuex.Store({
  state,
  actions,
  mutations,
  getters,
});


export default store;
export {
  store,
  types,
};
